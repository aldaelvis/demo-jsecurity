package com.example.demo1jwt.service;

import com.example.demo1jwt.model.Usuario;

import java.util.ArrayList;
import java.util.List;

public class UsuarioService {

    List<Usuario> usuarioList = new ArrayList<>();

    public List<Usuario> listarUsuarios() {
        return usuarioList;
    }

    public void addUser(Usuario usuario) {
        usuarioList.add(usuario);
    }

    public Usuario buscarUsuario(int id) {

        Usuario usuario = usuarioList.stream()
                .filter(u -> u.getId() == id)
                .findAny()
                .orElse(null);

        return usuario;
    }

    public Usuario iniciarSesion(String username, String password) {

        Usuario usuario = usuarioList.stream()
                .filter(u -> u.getUsername().equals(username) && u.getPassword().equals(password))
                .findAny()
                .orElse(null);

        return usuario;

    }


}
