package com.example.demo1jwt;

import com.example.demo1jwt.model.Usuario;
import com.example.demo1jwt.security.CypherService;
import com.example.demo1jwt.service.UsuarioService;
import com.example.demo1jwt.viewmodel.LoginViewModel;

import javax.annotation.PostConstruct;
import javax.annotation.security.RolesAllowed;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.security.PrivateKey;
import java.util.*;

@ApplicationScoped
@Path("/user")
public class UsuarioResource {

    @Inject
    private UsuarioService usuarioService;

    @Inject
    private CypherService cypherService;

    PrivateKey key;


    @PostConstruct
    public void init() {
        try {
            key = cypherService.readPrivateKey();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /*@Inject
    @Claim("groups")
    private JsonArray roles;*/

    @GET
    public Response listarUsuarios() {
        return Response.ok(usuarioService.listarUsuarios()).build();
    }

    @POST
    @Path("/signup")
    public Response registrarme(Usuario usuario) {
        usuarioService.addUser(usuario);

        return Response.ok().build();
    }

    /*@POST
    @Path("/login")
    public Response iniciarSesion(LoginViewModel viewModel) {
        Usuario usuario = usuarioService.iniciarSesion(viewModel.getUsername(), viewModel.getPassword());
        Map<String, Object> messages = new HashMap<>();

        if (usuario == null) {
            messages.put("message", "Credenciales invalidas");

            return Response.status(Response.Status.FORBIDDEN)
                    .entity(messages)
                    .build();
        }

        return Response.ok(usuario).build();
    }*/

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response login(@FormParam("username") String username, @FormParam("password") String password) {
        Usuario usuario = usuarioService.iniciarSesion(username, password);
        Map<String, Object> messages = new HashMap<>();

        if (usuario == null) {
            messages.put("message", "Credenciales invalidas");

            return Response.status(Response.Status.FORBIDDEN)
                    .entity(messages)
                    .build();
        }

        String token = cypherService.generateJWTString(key, username, usuario.getRoles());
        messages.put("usuario", usuario);
        messages.put("token", "Bearer ".concat(token));

        return Response.ok(messages).build();
    }


    @GET
    @Path("/os")
    @RolesAllowed("ROLE_ADMIN")
    public Response getOs() {
        return Response.ok(System.getProperties().getProperty("os.name")).build();
    }


}
